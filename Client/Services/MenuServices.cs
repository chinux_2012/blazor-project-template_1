﻿using BlazorComponent;
using Client.Dtos;
using Client.Entities;
using Client.Repositorys;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services
{
  public class MenuServices
  {
    private readonly BaseRepository<MenuEntity> _menuRepo;
    public MenuServices(BaseRepository<MenuEntity> menuRepo)
    {
      _menuRepo = menuRepo;
    }

    /**
     * 获取所有菜单 平行
     */
    public Task<List<MenuEntity>> ListAsync()
    {
      return _menuRepo.GetListAsync(it => it.IsDeleted == 0);
    }

    /**
     * 获取所有菜单 树
     */
    public List<MenuEntity> Tree(int parentId = 0)
    {
      var query = _menuRepo.Context.Queryable<MenuEntity>()
        .Where(it => it.IsDeleted == 0)
        .WhereIF(parentId != 0, it => it.ParentId == parentId)
        .OrderBy(it => it.Order);

      return query.ToList().Select(node =>
      {
        node.Children = Tree(node.Id);
        return node;
      }).ToList();
    }

    /**
     * 新增菜单
     */
    public async Task<bool> AddAsync(MenuEntity menuEntity)
    {
      return await _menuRepo.InsertAsync(menuEntity);
    }
  }
}
