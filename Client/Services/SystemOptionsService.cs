﻿using Client.Entities;
using Client.Repositorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Services
{
  public class SystemOptionsService
  {
    BaseRepository<SystemOptionsEntity> _systemOptionRepo;

    public SystemOptionsService(BaseRepository<SystemOptionsEntity> systemOptionRepo)
    {
      _systemOptionRepo = systemOptionRepo;
    }

    /**
     * 获取所有系统配置选项
     */
    public Task<List<SystemOptionsEntity>> ListAsync()
    {
      return _systemOptionRepo.GetListAsync();
    }
  }
}
