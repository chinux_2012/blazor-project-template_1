﻿using Masa.Blazor;
using Masa.Blazor.Presets;
using Microsoft.Extensions.DependencyInjection;
using Photino.Blazor;
using Client;
using Client.Repositorys;
using Client.Entities;
using Client.Services;
using Blazor.Extensions.Logging;
using Microsoft.Extensions.Logging;
using System.IO;

/**
 * ui文档 https://docs.masastack.com/blazor/components/application#section-9ed88ba45e94752868078bb0 
 */

internal class Program
{
  [STAThread]
  private static void Main(string[] args)
  {
    InitUI(args);
  }

  
  public static void InitUI(string[] args)
  {
    // client
    var appBuilder = PhotinoBlazorAppBuilder.CreateDefault(args);

    appBuilder.RootComponents.Add<App>("#app");
    appBuilder.Services.AddMasaBlazor(options =>
    {
      options.ConfigureIcons(IconSet.MaterialDesignIcons);
      options.Defaults = new Dictionary<string, IDictionary<string, object?>?>()
      {
        {
          PopupComponents.SNACKBAR, new Dictionary<string, object?>()
          {
            { nameof(PEnqueuedSnackbars.Closeable), true },
            { nameof(PEnqueuedSnackbars.Position), SnackPosition.TopCenter }
          }
        }
      };
      options.ConfigureTheme(theme =>
      {
        // theme.Dark = true;
      });
    });

    appBuilder.Services
      .AddLogging(builder =>
      {
          builder.AddBrowserConsole()
           .AddConsole()
           .AddDebug()
           ;
      })
      .AddSingleton<GlobalVarService>()
      .AddScoped<MenuServices>()
      .AddScoped(typeof(BaseRepository<>)); // 注册仓储


    var app = appBuilder.Build();


    app.MainWindow.SetTitle("Client")
    // .SetMaximized(true)
    .SetUseOsDefaultSize(false)
    //.SetSize(1300, 700)
    .SetMaximized(true)
    .SetIconFile(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/assets/favicon.ico"))
    //.SetIconFile("/wwwroot/assets/logo.png");
    //.SetIconFile(Path.Combine( "/assets/logo.png"))
    ;


    app.MainWindow.RegisterWindowCreatedHandler((object? sender, EventArgs e) =>
    {
      BaseRepository<SystemOptionsEntity> systemOptionsRepo = new BaseRepository<SystemOptionsEntity>();
      try
      {
        // 建库
        systemOptionsRepo.AsSugarClient().DbMaintenance.CreateDatabase();
        // 建表
        systemOptionsRepo.AsSugarClient().CodeFirst.InitTables<SystemOptionsEntity>();
        systemOptionsRepo.AsSugarClient().CodeFirst.InitTables<MenuEntity>();
      }
      catch (Exception ex)
      {
        throw;
      }

    });

    AppDomain.CurrentDomain.UnhandledException += (sender, error) =>
    {
    };


    app.Run();
  }

  
}