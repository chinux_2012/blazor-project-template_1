﻿using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Entities
{
  [SugarTable("Menu")]
  public class MenuEntity : Base
  {
    public string Name { get; set; }

    public string Route { get; set; }

    [SugarColumn(IsIgnore = true)]
    public List<MenuEntity>? Children { get; set; }

    [SugarColumn(DefaultValue = "1")]
    public int Order { get; set; }

    /*
    [SugarColumn(IsNullable = true)]
    public List<MenuEntity> Children { get; set;}
    */

    [SugarColumn(IsNullable = true)]
    public int ParentId { get; set; }

    [SugarColumn(IsNullable = true)]
    public string? Icon { get; set; }

    [SugarColumn(IsNullable = true)]
    public string? Remark { get; set; }
  }
}
