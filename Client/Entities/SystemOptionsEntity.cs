﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Entities
{
  [SugarTable("SystemOptions")]
  public class SystemOptionsEntity : Base
  {
    public string Key { get; set; }
    public string Value { get; set; }

    [SugarColumn(IsNullable = true)]
    public string? Remark { get; set; }
  }
}
