﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Entities
{
  public abstract class Base
  {
    [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
    public virtual int Id { get; set; }

    [SugarColumn(DefaultValue = "0")]
    public virtual int IsDeleted { get; set; }

    [SugarColumn(IsNullable = true, InsertServerTime = true)]
    public virtual DateTime CreatedTime { get; set; }

    [SugarColumn(IsNullable = true, UpdateServerTime = true)]
    public virtual DateTime? UpdatedTime { get; set; }
  }
}
