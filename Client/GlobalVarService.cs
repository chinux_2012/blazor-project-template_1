﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
  public class MyMenuItem
  {
    public string Text { get; set; }
    public string Icon { get; set; }
  }
  public class NavMenuItem
  {
    public string Icon { get; set; }
    public string Name { get; set; }
    public string RoutePath { get; set; }
    public int Level { get; set; } = 1;
    public List<NavMenuItem>? Children { get; set; }
  }

  public class GlobalVarService
  {
    // 当前导航数据
    public NavMenuItem CurrentNavItem { get; set; } = new NavMenuItem { Icon = "", Name = "", RoutePath = "", Level = 1 };

    // 导航菜单
    public List<NavMenuItem>  NavMenuList { get; set; } = new List<NavMenuItem>
    {
      new NavMenuItem{Icon="mdi-home", Name="首页", RoutePath="/", Level = 1},
      new NavMenuItem{Icon="mdi-plus", Name="Counter", RoutePath="/counter", Level = 1},
      new NavMenuItem{Icon="mdi-list-box", Name="Fetch data", RoutePath="/fetchdata", Level = 1},
      new NavMenuItem{Icon="mdi-cogs", Name="系统设置", RoutePath="", Level = 1, Children = new List<NavMenuItem>()
        {
           new NavMenuItem{ Icon="mdi-menu", Name="菜单", RoutePath="/systemSetting/menu", Level = 2 },
           new NavMenuItem{ Icon="mdi-account-settings", Name="用户", RoutePath="/systemSetting/user", Level = 2 },
        }
      },
    };
    /**
    * [递归]根据RoutePath匹配的导航菜单项
    */
    public NavMenuItem GetNavMenuItemByKey(List<NavMenuItem> tree, string routePath)
    {
        NavMenuItem ret = new NavMenuItem { Icon="", Name="", RoutePath="", Children=null };
        foreach (var item in tree)
        {
            if (item.RoutePath == routePath)
            {
                ret = item;
            } 
            if (item.Children != null)
            {
                GetNavMenuItemByKey(item.Children, routePath);
            }
        }
        return ret;
    }

    // 我的菜单
    public List<MyMenuItem> MyMenuList { get; set; } = new List<MyMenuItem>
    {
       new MyMenuItem { Text= "My Files", Icon= "mdi-folder" },
       new MyMenuItem { Text= "Shared with me", Icon= "mdi-account-multiple" },
       new MyMenuItem { Text= "Starred", Icon= "mdi-star" },
       new MyMenuItem { Text= "Recent", Icon= "mdi-history" },
       new MyMenuItem { Text= "Offline", Icon= "mdi-check-circle" },
       new MyMenuItem { Text= "Uploads", Icon= "mdi-upload" },
       new MyMenuItem { Text= "Backups", Icon= "mdi-cloud-upload" }
    };

    

    }
}
