﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Repositorys
{
  public class BaseRepository<TEntity> : SimpleClient<TEntity> where TEntity : class, new()
  {
    public BaseRepository(ISqlSugarClient? context = null) : base(context)//注意这里要有默认值等于null
    {
      if (context == null)
      {
        base.Context = new SqlSugarClient(new ConnectionConfig()
        {
          DbType = DbType.Sqlite,
          InitKeyType = InitKeyType.Attribute,
          IsAutoCloseConnection = true,
          ConnectionString = DBConfig.ConnectionString
        });

        base.Context.Aop.OnLogExecuting = (s, p) =>
        {
          Console.WriteLine(s);
        };
      }
    }
    public class DBConfig
    {
      private static string GetCurrentProjectPath
      {

        get
        {
          // return Environment.CurrentDirectory.Replace(@"\bin\Debug", "");//获取具体路径
          return Environment.CurrentDirectory;
        }
      }
      public static string ConnectionString = @"DataSource=" + GetCurrentProjectPath + @"\System.db";
    }
  }
}
